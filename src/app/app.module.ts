import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, PreloadAllModules } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AgmCoreModule } from '@agm/core';
import { GalleryModule } from '@ngx-gallery/core';
import { LightboxModule } from '@ngx-gallery/lightbox';
import { GallerizeModule } from '@ngx-gallery/gallerize';
import { MomentModule } from 'ngx-moment';

import 'jquery';
import 'bootstrap/dist/js/bootstrap.bundle';

/*
 * Platform and Environment providers/directives/pipes
 */
import { environment } from 'environments/environment';
import { ROUTES } from './app.routes';
// App is our top level component
import { AppComponent } from './app.component';
import { APP_RESOLVER_PROVIDERS } from './app.resolver';
import { AppState, InternalStateType } from './app.service';
import { HouseService } from './shared/services/house.service';
import { NoContentComponent } from './no-content';
import { AvatarComponent } from './avatar';

import '../styles/styles.scss';
import { OperatorService } from './shared/services/operator.service';
import { MessagesService } from './shared/services/messages.service';
import { NotificationsService } from './shared/services/notifications.service';
import { SharedModule } from './shared';
import { TripService } from './shared/services/trip.service';
import { UserService } from './shared/services/user.service';
import { DataService } from './shared/services/data.service';

// Application wide providers
const APP_PROVIDERS = [
  ...APP_RESOLVER_PROVIDERS,
  AppState,
  HouseService,
  OperatorService,
  MessagesService,
  NotificationsService,
  TripService,
  UserService,
  DataService
];

interface StoreType {
  state: InternalStateType;
  restoreInputValues: () => void;
  disposeOldHosts: () => void;
}

/**
 * `AppModule` is the main entry point into Angular2's bootstraping process
 */
@NgModule({
  bootstrap: [AppComponent],
  declarations: [
    AppComponent,
    AvatarComponent,
    NoContentComponent,
  ],
  /**
   * Import Angular's modules.
   */
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(ROUTES, {
      useHash: Boolean(history.pushState) === false,
      preloadingStrategy: PreloadAllModules
    }),
    NgbModule.forRoot(),
    AgmCoreModule.forRoot({
      // please get your own API key here:
      // https://developers.google.com/maps/documentation/javascript/get-api-key?hl=en
      apiKey: 'AIzaSyCdTYROnWVzLq2YD5XnB6Vt14KgKOtQc5o'
    }),
    GalleryModule.forRoot({
      counter: false,
      loop: true
    }),
    LightboxModule.forRoot(),
    GallerizeModule,
    MomentModule,
    SharedModule,
  ],
  /**
   * Expose our Services and Providers into Angular's dependency injection.
   */
  providers: [
    environment.ENV_PROVIDERS,
    APP_PROVIDERS
  ]
})
export class AppModule {
}
