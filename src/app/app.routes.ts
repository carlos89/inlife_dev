import { Routes } from '@angular/router';
import { NoContentComponent } from './no-content';

export const ROUTES: Routes = [
  {path: 'dashboard', loadChildren: 'app/dashboard#DashboardModule'},
  {path: 'sign-in', loadChildren: 'app/sign-in#SignInModule'},
  {path: '', redirectTo: 'dashboard', pathMatch: 'full'},
  {path: '**', component: NoContentComponent},
];
