import { Component, Input } from '@angular/core';

@Component({
  selector: 'avatar',
  styleUrls: ['./avatar.component.scss'],
  templateUrl: './avatar.component.html'
})

export class AvatarComponent {
  @Input() public image: string;
  @Input() public icon: string;
}
