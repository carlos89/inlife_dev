import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { routes } from './dashboard.routes';
import { HousesComponent } from './houses';
import { DashboardComponent } from './dashboard.component';
import { HousingTripComponent } from './housing-trip';
import { InlifeAdvisorComponent } from './inlife-advisor';
import { OperatorComponent } from './operator';
import { ProfileComponent } from './profile';
import { NouisliderModule } from 'ng2-nouislider';
import { HouseComponent } from './houses/house';
import { AgmCoreModule } from '@agm/core';
import { GalleryModule } from '@ngx-gallery/core';
import { LightboxModule } from '@ngx-gallery/lightbox';
import { GallerizeModule } from '@ngx-gallery/gallerize';
import { MomentModule } from 'ngx-moment';
import { SharedModule } from '../shared';

@NgModule({
  declarations: [
    /**
     * Components / Directives/ Pipes
     */
    HouseComponent,
    HousesComponent,
    DashboardComponent,
    HousingTripComponent,
    InlifeAdvisorComponent,
    OperatorComponent,
    ProfileComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    NgbModule,
    NouisliderModule,
    AgmCoreModule,
    GalleryModule,
    LightboxModule,
    GallerizeModule,
    MomentModule,
    SharedModule
  ],
})
export class DashboardModule {
  public static routes = routes;
}
