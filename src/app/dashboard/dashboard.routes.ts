import { DashboardComponent } from './dashboard.component';
import { HousesComponent } from './houses';
import { HousingTripComponent } from './housing-trip';
import { InlifeAdvisorComponent } from './inlife-advisor';
import { OperatorComponent } from './operator';
import { HouseComponent } from './houses/house';
import { ProfileComponent } from './profile';

export const routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'houses'
      },
      {
        path: 'houses',
        children: [
          {
            path: '',
            pathMatch: 'full',
            component: HousesComponent,
          },
          {
            path: 'house',
            component: HouseComponent,
          }
        ],
      },
      {
        path: 'housing-trip',
        component: HousingTripComponent
      },
      {
        path: 'inlife-advisor',
        component: InlifeAdvisorComponent
      },
      {
        path: 'operator',
        component: OperatorComponent
      },
      {
        path: 'profile',
        component: ProfileComponent,
      }
    ]
  },
];
