import { Component, OnInit } from '@angular/core';
import { Gallery, GalleryConfig, GalleryItem, GalleryRef, ImageItem } from '@ngx-gallery/core';
import { IHouse } from '../../../shared/models/house.model';
import { IFile } from '../../../shared/models/file.model';
import { HouseService } from '../../../shared/services/house.service';

@Component({
  selector: 'house',
  styleUrls: ['./house.component.scss'],
  templateUrl: './house.component.html',
})
export class HouseComponent implements OnInit {
  public house: IHouse = null;

  public getExpiryTimer = HouseService.getExpiryTimer;

  public galleryIdMobile: string = 'galleryMobile';
  public galleryIdDesktop: string = 'galleryDesktop';
  public galleryConfigMobile: GalleryConfig = {
    thumbPosition: 'bottom',
    thumbHeight: 90,
    thumbWidth: 144,
  };
  public galleryConfigDesktop: GalleryConfig = {
    thumbPosition: 'right',
    thumbHeight: 80,
    thumbWidth: 152,
  };

  // google maps zoom level
  public zoom: number = 13;

  public constructor(private gallery: Gallery, private houseService: HouseService) {
  }

  public ngOnInit() {
    this.house = this.houseService.getHouse();

    const galleryMobileRef: GalleryRef = this.gallery.ref(this.galleryIdMobile);
    const galleryDesktopRef: GalleryRef = this.gallery.ref(this.galleryIdDesktop);

    // Set gallery items array
    const galleryImages: GalleryItem[] = this.house.images.map((image: IFile) => {
      return new ImageItem(image.url, image.url);
    });

    galleryMobileRef.load(galleryImages);
    galleryDesktopRef.load(galleryImages);

    window.setTimeout(() => {
      galleryMobileRef.next();
      galleryMobileRef.prev();
      galleryDesktopRef.next();
      galleryDesktopRef.prev();
    }, 0);
  }
}
