import { Component, OnInit, } from '@angular/core';
import { IHouse } from '../../shared/models/house.model';
import { HouseService } from '../../shared/services/house.service';
import { take } from 'lodash';

@Component({
  selector: 'houses',
  styleUrls: ['./houses.component.scss'],
  templateUrl: './houses.component.html',
})
export class HousesComponent implements OnInit {
  public houses: IHouse[] = null;

  public getExpiryTimer = HouseService.getExpiryTimer;

  public constructor(private houseService: HouseService) {
  }

  public ngOnInit() {
    const houses = this.houseService.getHouses(3);

    houses.map((house) => {
      house.images = take(house.images, 3);
      return house;
    });

    this.houses = houses;
  }
}
