import { Component, OnInit } from '@angular/core';
import { IHousingTrip } from '../../shared/models/trip.model';
import { TripService } from '../../shared/services/trip.service';
import { HouseService } from '../../shared/services/house.service';
import { IHouseFeature } from '../../shared/models/house.model';
import { DataService } from '../../shared/services/data.service';
import { ITestimonial } from '../../shared/models/testimonial.model';

@Component({
  selector: 'housing-trip',
  styleUrls: ['./housing-trip.component.scss'],
  templateUrl: './housing-trip.component.html',
})
export class HousingTripComponent implements OnInit {
  public houseFeatures: IHouseFeature[] = null;
  public housingTrip: IHousingTrip = null;
  public testimonials: ITestimonial[] = [];

  constructor(private tripService: TripService,
              private houseService: HouseService,
              private dataService: DataService) {
  }

  public ngOnInit() {
    this.housingTrip = this.tripService.getHousingTrip();
    this.houseFeatures = this.houseService.getHouseFeatures();
    this.testimonials = this.dataService.getTestimonials();
  }
}
