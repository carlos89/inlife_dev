import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { TripService } from '../../shared/services/trip.service';
import { IVirtualTrip } from '../../shared/models/trip.model';
import { IHouseFeature } from '../../shared/models/house.model';
import { HouseService } from '../../shared/services/house.service';

@Component({
  selector: 'inlife-advisor',
  styleUrls: ['./inlife-advisor.component.scss'],
  templateUrl: './inlife-advisor.component.html',
})
export class InlifeAdvisorComponent implements OnInit {
  @ViewChild('openModal') public openModal: ElementRef;
  public houseFeatures: IHouseFeature[] = null;
  public title = 'app works!';
  public virtualTrip: IVirtualTrip = null;

  constructor(private tripService: TripService,
              private houseService: HouseService) {
  }

  public ngOnInit() {
    this.openModal.nativeElement.click();
    this.houseFeatures = this.houseService.getHouseFeatures();
    this.virtualTrip = this.tripService.getVirtualTrip();
  }
}
