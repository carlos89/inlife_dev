import { Component, OnInit } from '@angular/core';
import { IOperator } from '../../shared/models/operator.model';
import { OperatorService } from '../../shared/services/operator.service';
import { IMessage } from '../../shared/models/message.modal';
import { MessagesService } from '../../shared/services/messages.service';

enum Enum {
  facebook = 'fa-facebook-f',
  twitter = 'fa-twitter',
  whatsapp = 'fa-whatsapp'
}

@Component({
  selector: 'operator',
  styleUrls: ['./operator.component.scss'],
  templateUrl: './operator.component.html',
  
})
export class OperatorComponent implements OnInit {
  public operator: IOperator = null;
  public messages: IMessage[] = [];

  public constructor(private operatorService: OperatorService,
                     private messagesService: MessagesService) {
  }

  public ngOnInit() {
    this.operator = this.operatorService.getOperator();
    this.messages = this.messagesService.getMessages();
  }

  public getSocialLinkIcon(name: string): string {
    return Enum[name];
  }

  public isCurrentUser(id: number): boolean {
    return id === 1;
  }
}
