import { Component, OnInit } from '@angular/core';
import { NotificationsService } from '../../shared/services/notifications.service';
import { TripService } from '../../shared/services/trip.service';
import { IVirtualTrip } from '../../shared/models/trip.model';
import { UserService } from '../../shared/services/user.service';
import { DataService } from '../../shared/services/data.service';
import { IUser } from '../../shared/models/person.model';

@Component({
  selector: 'profile',
  styleUrls: ['./profile.component.scss'],
  templateUrl: './profile.component.html'
})
export class ProfileComponent implements OnInit {
  public notifications = [];
  public virtualTrip: IVirtualTrip = null;
  public user: IUser = null;
  public countries: any[] = [];
  public regions: any[] = [];
  public universities: any[] = [];

  constructor(private notificationsService: NotificationsService,
              private tripService: TripService,
              private userService: UserService,
              private dataService: DataService) {
  }

  public ngOnInit() {
    this.notifications = this.notificationsService.getNotifications();
    this.virtualTrip = this.tripService.getVirtualTrip();
    this.user = this.userService.getUser();
    this.countries = this.dataService.getCountries();
    this.regions = this.dataService.getRegions();
    this.universities = this.dataService.getUniversities();
  }
}
