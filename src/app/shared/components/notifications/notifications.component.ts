import { Component, OnInit } from '@angular/core';
import { NotificationsService } from '../../services/notifications.service';
import { some } from 'lodash';

@Component({
  selector: 'notifications',
  templateUrl: './notifications.component.html'
})
export class NotificationsComponent implements OnInit {
  public notifications = [];

  constructor(private notificationsService: NotificationsService) {
  }

  public ngOnInit() {
    this.notifications = this.notificationsService.getNotifications();
  }

  public getPopoverClass() {
    let popoverClass = 'notifications-popover';

    if (this.hasNotifications()) {
      popoverClass += '  has-notifications';
    }

    return popoverClass;
  }

  public hasNotifications() {
    return some(this.notifications, ['read', false]);
  }
}
