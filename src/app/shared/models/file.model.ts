import { IWebLink } from './weblink.model';

export interface IFile extends IWebLink {
  type?: string;
}
