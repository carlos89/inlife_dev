import { IPerson } from './person.model';
import { IFile } from './file.model';

export interface IHouseDetail {
  icon: string;
  description: string;
}

export interface IPosition {
  latitude: number;
  longitude: number;
}

export interface IAddress {
  address?: string;
  country?: string;
  position?: IPosition;
}

export interface IHouseFeature {
  id: string;
  name: string;
  image: string;
}

export interface IHouse {
  amenities?: IHouseFeature[];
  description?: string;
  details?: IHouseDetail[];
  expiryDate: Date;
  images?: IFile[];
  landlord: IPerson;
  location: IAddress;
  price: number;
  roommates?: IPerson[];
  rules?: IHouseFeature[];
  summary: string;
  title: string;
}
