import { IPerson } from './person.model';

export interface IMessage {
  content: any;
  from: IPerson;
  time: number;
  to: IPerson;
}
