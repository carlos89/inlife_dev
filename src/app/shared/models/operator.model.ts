import { IPerson } from './person.model';
import { IWebLink } from './weblink.model';

export interface IOperator extends IPerson {
  description?: string;
  interests?: string;
  isOnline: boolean;
  social?: IWebLink[];
}
