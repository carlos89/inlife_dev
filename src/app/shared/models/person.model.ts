import { IFile } from './file.model';

export interface IPerson {
  avatar?: IFile;
  gender?: string;
  id?: number;
  name: string;
  phone?: number;
}

export interface IUser extends IPerson {
  email?: string;
  nationality?: string;
  region?: string;
  university?: string;
}
