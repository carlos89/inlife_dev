import { IUser } from './person.model';

export interface ITestimonial {
  person: IUser;
  title: string;
  text: string;
}
