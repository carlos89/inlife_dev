import { IHouse, IHouseFeature } from './house.model';
import { IPerson } from './person.model';

export interface ITrip {
  housePreferences: IHousePreferences;
  moveInDay: number;
  tripDay: number;
}

export interface IHouseStop {
  date: number;
  house: IHouse;
}

export interface IVirtualTrip extends ITrip {
  stops: IHouseStop[];
}

export interface IHousingTrip extends ITrip {
  roommates: IPerson[];
  stop: IHouseStop;
}

export interface IHousePreferences {
  accommodationType: string;
  bedrooms: number;
  budget: number;
  houseFeatures: IHouseFeature[];
  months: number;
  numberOfPeople: number;
  preferredLocation: string;
}
