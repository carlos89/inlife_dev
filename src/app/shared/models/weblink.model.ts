export interface IWebLink {
  name?: string;
  url: string;
}
