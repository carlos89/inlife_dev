import { Injectable } from '@angular/core';
import { ITestimonial } from '../models/testimonial.model';

@Injectable()
export class DataService {
  private countries: object[] = [
    {
      label: 'Portugal',
      value: '0'
    },
    {
      label: 'England',
      value: '1'
    },
    {
      label: 'France',
      value: '2'
    }
  ];

  private regions: object[] = [
    {
      label: 'Lisbon',
      value: '0'
    },
    {
      label: 'Porto',
      value: '1'
    },
    {
      label: 'Faro',
      value: '2'
    }
  ];

  private universities: object[] = [
    {
      label: 'FCT',
      value: '0'
    },
    {
      label: 'ISCTE',
      value: '1'
    },
    {
      label: 'IST',
      value: '2'
    }
  ];

  private testimonials: ITestimonial[] = [
    {
      person: {
        avatar: {
          url: 'https://randomuser.me/api/portraits/men/40.jpg'
        },
        name: 'Eve Robitaille',
        nationality: 'Canada',
      },
      title: 'Housing Trip was my best choice',
      text: '“Inlife gave me full support, since I arrived at the airport until the day of my departure. I actually became good friends with my Inlife operator, who visited the apartments with…me and helped me decide which option was the best for me. I could always count on her to help me if I had issues with my landlord or even about transportation and other bureaucracies. Inlife is one big family!”',
    },
    {
      person: {
        avatar: {
          url: 'https://randomuser.me/api/portraits/men/41.jpg'
        },
        name: 'Francesco Ricci',
        nationality: 'Italy',
      },
      title: 'Housing Trip was my best choice',
      text: '“With Inlife, I got the chance to learn everything about my future apartment and housemates in advance, while I was still in Denmark. I made sure I could have friends over during my stay and clarified other issues related to the contract rules. It made all the difference! Totally recommend it.”'
    },
    {
      person: {
        avatar: {
          url: 'https://randomuser.me/api/portraits/men/40.jpg'
        },
        name: 'Viviana Vega Reyes',
        nationality: 'Chile',
      },
      title: 'Housing Trip was my best choice',
      text: '“In the beginning I was having a lot of trouble finding good accommodation in Lisbon. I like to make wise choices, and the idea of not being able to visit the places in advance never made much sense to me.Later, I found Inlife’s housing trip service and contacted them. They quickly showed me just what I needed.I ended up in a great place, and my only regret is not having discovered this service earlier. It just made my life so much easier!”',
    }
  ];

  public getCountries(): object[] {
    return this.countries;
  }

  public getRegions(): object[] {
    return this.regions;
  }

  public getUniversities(): object[] {
    return this.universities;
  }

  public getTestimonials(): ITestimonial[] {
    return this.testimonials;
  }
}
