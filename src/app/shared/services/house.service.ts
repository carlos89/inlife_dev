import { Injectable } from '@angular/core';
import { IHouse, IHouseFeature } from '../models/house.model';

@Injectable()
export class HouseService {
  public static getExpiryTimer(date: Date): string {
    return date.getHours() + 'h' + date.getMinutes();
  }

  private houseFeatures: IHouseFeature[] = [
    {
      id: 'doubleBed',
      name: 'Double Bed',
      image: 'assets/icon/double_bed.png'
    },
    {
      id: 'fullKitchen',
      name: 'Full Kitchen',
      image: 'assets/icon/kitchen.png'
    },
    {
      id: 'balcony',
      name: 'Balcony',
      image: 'assets/icon/balcony.png'
    },
    {
      id: 'privateBathroom',
      name: 'Private Bathroom',
      image: 'assets/icon/bathroom.png'
    },
    {
      id: 'pets',
      name: 'Pets',
      image: 'assets/icon/dog.png'
    },
    {
      id: ' smoking',
      name: 'Smoking',
      image: 'assets/icon/smoking.png'
    },
    {
      id: 'overnight',
      name: 'Overnight',
      image: 'assets/icon/overnight.png'
    }
  ];

  public getHouse(): IHouse {
    return {
      amenities: [
        {
          id: 'doubleBed',
          name: 'Double Bed',
          image: 'assets/icon/double_bed.png',
        },
        {
          id: 'fullKitchen',
          name: 'Full Kitchen',
          image: 'assets/icon/kitchen.png'
        },
        {
          id: 'privateBathroom',
          name: 'Private Bathroom',
          image: 'assets/icon/bathroom.png'
        },
      ],
      description: `<p>
        Donec facilisis tortor ut augue lacinia, at viverra est semper. Sed sapien metus,
        scelerisque nec pharetra id, tempor a tortor. Pellentesque non dignissim neque. Ut porta
        viverra est, ut dignissim elit elementum ut. Nunc vel rhoncus nibh, ut tincidunt turpis.
        Integer ac enim pellentesque, adipiscing metus id, pharetra odio. Donec bibendum nunc sit
        amet tortor scelerisque luctus et sit amet mauris. Suspendisse felis sem, condimentum
        ullamcorper est sit amet, molestie mollis nulla. Etiam lorem orci, consequat ac magna quis,
        facilisis vehicula neque.
      </p>
      <p>
        Donec facilisis tortor ut augue lacinia, at viverra est semper. Sed sapien metus,
        scelerisque nec pharetra id, tempor a tortor. Pellentesque non dignissim neque. Ut porta
        viverra est, ut dignissim elit elementum ut. Nunc vel rhoncus nibh, ut tincidunt turpis.
        Integer ac enim pellentesque, adipiscing metus id, pharetra odio.
      </p>`,
      details: [
        {
          icon: 'fas fa-map',
          description: '100m2',
        },
        {
          icon: 'fas fa-bath',
          description: '3 bathrooms',
        },
        {
          icon: 'fas fa-bed',
          description: '4 bedrooms',
        },
      ],
      expiryDate: new Date(),
      images: [
        {
          url: 'https://picsum.photos/445/270/?random'
        },
        {
          url: 'https://picsum.photos/445/270/?random'
        },
        {
          url: 'https://picsum.photos/445/270/?random'
        },
        {
          url: 'https://picsum.photos/445/270/?random'
        },
        {
          url: 'https://picsum.photos/445/270/?random'
        },
        {
          url: 'https://picsum.photos/445/270/?random'
        },
        {
          url: 'https://picsum.photos/445/270/?random'
        },
        {
          url: 'https://picsum.photos/445/270/?random'
        },
        {
          url: 'https://picsum.photos/445/270/?random'
        },
        {
          url: 'https://picsum.photos/445/270/?random'
        },
        {
          url: 'https://picsum.photos/445/270/?random'
        },
        {
          url: 'https://picsum.photos/445/270/?random'
        }
      ],
      landlord: {
        name: 'Patricia',
        avatar: {
          url: 'https://randomuser.me/api/portraits/women/53.jpg'
        },
      },
      location: {
        position: {
          latitude: 38.7436883,
          longitude: -9.1952226,
        }
      },
      price: 400,
      roommates: [
        {
          name: 'John',
          avatar: {
            url: 'https://randomuser.me/api/portraits/men/46.jpg'
          }
        },
        {
          name: 'John',
          avatar: {
            url: 'https://randomuser.me/api/portraits/men/81.jpg'
          }
        },
        {
          name: 'John',
          avatar: {
            url: 'https://randomuser.me/api/portraits/men/54.jpg'
          }
        },
      ],
      rules: [
        {
          id: ' smoking',
          name: 'Smoking',
          image: 'assets/icon/smoking.png'
        },
      ],
      summary: 'If you come to Lisbon, this apartment can be a good opportunity! Here you will ' +
      'find various cultural attractions, typical cafes.',
      title: 'Apartment with 4 bedrooms',
    };
  }

  public getHouseFeatures(): IHouseFeature[] {
    return this.houseFeatures;
  }

  public getHouses(quantity: number): IHouse[] {
    const houses = new Array(quantity);

    houses.fill(this.getHouse());

    return houses;
  }
}
