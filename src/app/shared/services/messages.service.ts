import { IMessage } from '../models/message.modal';
import { Injectable } from '@angular/core';

@Injectable()
export class MessagesService {
  private readonly pageSize: number = 10;
  private messages: IMessage[] = [];
  private sampleMessages: IMessage[] = [
    {
      content: 'Hello, my name is Tiago, I’m your operator. Do you have any question about ' +
        'your Housing Trip?',
      from: {
        avatar: {
          url: 'https://randomuser.me/api/portraits/men/43.jpg',
          name: 'Tiago Faria',
        },
        name: 'Tiago Faria',
        id: 0,
      },
      time: Date.now(),
      to: {
        avatar: {
          url: 'https://randomuser.me/api/portraits/women/53.jpg',
          name: 'Patricia Mendonza',
        },
        name: 'Patricia Mendonza',
        id: 1,
      }
    },
    {
      content: 'Mauris non tempor quam, et lacinia sapien. Mauris accumsan eros eget libero ' +
        'posuere vulputate.',
      from: {
        avatar: {
          url: 'https://randomuser.me/api/portraits/women/53.jpg',
          name: 'Patricia Mendonza',
        },
        name: 'Patricia Mendonza',
        id: 1,
      },
      time: Date.now(),
      to: {
        avatar: {
          url: 'https://randomuser.me/api/portraits/men/43.jpg',
          name: 'Tiago Faria',
        },
        name: 'Tiago Faria',
        id: 0,
      },
    },
    {
      content: 'Hello, my name is Tiago, I’m your operator. Do you have any question about ' +
        'your Housing Trip?',
      from: {
        avatar: {
          url: 'https://randomuser.me/api/portraits/men/43.jpg',
          name: 'Tiago Faria',
        },
        name: 'Tiago Faria',
        id: 0,
      },
      time: Date.now(),
      to: {
        avatar: {
          url: 'https://randomuser.me/api/portraits/women/53.jpg',
          name: 'Patricia Mendonza',
        },
        name: 'Patricia Mendonza',
        id: 1,
      }
    }
  ];

  constructor() {
    const size = 5 * this.pageSize;

    for (let i = 0; i < size;) {
      for (let j = 0; j < this.sampleMessages.length && i < size; j++) {
        const message = this.sampleMessages[j];
        this.messages.push(message);
        i++;
      }
    }
  }

  public getMessages(page: number = 1): IMessage[] {
    if (this.messages.length > (page * this.pageSize)) {
      return this.messages.slice((page - 1) * this.pageSize, page * this.pageSize);
    } else {
      return [];
    }
  }
}
