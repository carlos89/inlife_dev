import { Injectable } from '@angular/core';

@Injectable()
export class NotificationsService {
  public notifications: object[] = [
    {
      read: false,
      text: 'Cras justo odio Cras',
    },
    {
      read: true,
      text: 'Dapibus ac facilisis in',
    },
    {
      read: true,
      text: 'Morbi leo risus',
    },
    {
      read: true,
      text: 'Porta ac consectetur ac',
    },
    {
      read: true,
      text: 'Vestibulum at eros',
    },
  ];

  public getNotifications() {
    return this.notifications;
  }
}
