import { Injectable } from '@angular/core';
import { IOperator } from '../models/operator.model';

@Injectable()
export class OperatorService {
  public getOperator(): IOperator {
    return {
      name: 'Tiago Faria',
      avatar: {
        url: 'https://randomuser.me/api/portraits/men/43.jpg',
        name: 'Tiago Faria',
      },
      description: 'Cras quis nulla commodo, aliquam lectus sed, blandit augue. Cras ullamcorper ' +
        'bibendum bibendum. Duis tincidunt urna non pretium porta. Nam condimentum vitae ligula' +
        'vel ornare. Phasellus at semper turpis..',
      interests: 'Cras quis nulla commodo, aliquam lectus sed, blandit augue. Cras ullamcorper ' +
        'bibendum bibendum.',
      isOnline: true,
      social: [
        {
          name: 'facebook',
          url: 'https://facebook.com/',
        },
        {
          name: 'twitter',
          url: 'https://twitter.com/',
        },
        {
          name: 'whatsapp',
          url: 'https://whatsapp.com/',
        },
      ]
    };
  }
}
