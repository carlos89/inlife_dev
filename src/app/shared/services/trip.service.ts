import { Injectable } from '@angular/core';
import { IVirtualTrip, IHousingTrip } from '../models/trip.model';

@Injectable()
export class TripService {
  private housingTrip: IHousingTrip = {
    housePreferences: {
      accommodationType: 'bedroom',
      bedrooms: 1,
      budget: 300,
      houseFeatures: [],
      months: 6,
      numberOfPeople: 2,
      preferredLocation: 'Lisbon',
    },
    moveInDay: Date.now(),
    roommates: [
      {
        name: 'John',
        avatar: {
          url: 'https://randomuser.me/api/portraits/men/46.jpg'
        }
      },
      {
        name: 'John',
        avatar: {
          url: 'https://randomuser.me/api/portraits/men/81.jpg'
        }
      },
      {
        name: 'John',
        avatar: {
          url: 'https://randomuser.me/api/portraits/men/54.jpg'
        }
      },
    ],
    stop: {
      house: {
        title: 'Appartment with 2 bedrooms',
        expiryDate: new Date(),
        landlord: {
          name: 'Patricia',
          avatar: {
            url: 'https://randomuser.me/api/portraits/women/53.jpg'
          },
        },
        location: {
          position: {
            latitude: 38.7436883,
            longitude: -9.1952226,
          },
        },
        price: 200,
        summary: 'If you come to Lisbon, this apartment can be a good opportunity!',
      },
      date: Date.now()
    },
    tripDay: Date.now(),
  };
  private virtualTrip: IVirtualTrip = {
    housePreferences: {
      accommodationType: 'bedroom',
      bedrooms: 1,
      budget: 300,
      houseFeatures: [],
      months: 6,
      numberOfPeople: 2,
      preferredLocation: 'Lisbon',
    },
    moveInDay: Date.now(),
    stops: [
      {
        house: {
          title: 'Appartment with 2 bedrooms',
          expiryDate: new Date(),
          landlord: {
            name: 'Patricia',
            avatar: {
              url: 'https://randomuser.me/api/portraits/women/53.jpg'
            },
          },
          location: {
            position: {
              latitude: 38.7436883,
              longitude: -9.1952226,
            },
          },
          price: 200,
          summary: 'If you come to Lisbon, this apartment can be a good opportunity!',
        },
        date: Date.now()
      },
      {
        house: {
          title: 'Appartment in lovely Alfama',
          expiryDate: new Date(),
          landlord: {
            name: 'Patricia',
            avatar: {
              url: 'https://randomuser.me/api/portraits/women/53.jpg'
            },
          },
          location: {
            position: {
              latitude: 38.7436883,
              longitude: -9.1952226,
            },
          },
          price: 200,
          summary: 'If you come to Lisbon, this apartment can be a good opportunity!',
        },
        date: Date.now()
      },
      {
        house: {
          title: 'One Bedroom Appartment',
          expiryDate: new Date(),
          landlord: {
            name: 'Patricia',
            avatar: {
              url: 'https://randomuser.me/api/portraits/women/53.jpg'
            },
          },
          location: {
            position: {
              latitude: 38.7436883,
              longitude: -9.1952226,
            },
          },
          price: 200,
          summary: 'If you come to Lisbon, this apartment can be a good opportunity!',
        },
        date: Date.now()
      }
    ],
    tripDay: Date.now(),
  };

  public getHousingTrip(): IHousingTrip {
    return this.housingTrip;
  }

  public getVirtualTrip(): IVirtualTrip {
    return this.virtualTrip;
  }
}
