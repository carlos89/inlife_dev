import { Injectable } from '@angular/core';
import { IUser } from '../models/person.model';

@Injectable()
export class UserService {
  private user: IUser = {
    avatar: {
      url: 'https://randomuser.me/api/portraits/women/53.jpg',
      name: 'Patricia Mendonza',
    },
    name: 'Patricia Mendonza',
    id: 1,
  };

  public getUser(): IUser {
    return this.user;
  }
}
