import { NgModule } from '@angular/core';
import { NotificationsComponent } from './components/notifications';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
    NotificationsComponent
  ],
  imports: [
    CommonModule,
    NgbModule
  ],
  exports: [
    NotificationsComponent
  ]
})
export class SharedModule {
}
