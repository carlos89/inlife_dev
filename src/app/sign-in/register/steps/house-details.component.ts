import { Component, OnInit } from '@angular/core';
import { IHouseFeature } from '../../../shared/models/house.model';
import { HouseService } from '../../../shared/services/house.service';

@Component({
  templateUrl: './house-details.component.html',
  styleUrls: ['../register.component.scss'],
})
export class HouseDetailsComponent implements OnInit {
  public formData = {
    budget: 100,
  };

  public houseFeatures: IHouseFeature[] = [];

  constructor(private houseService: HouseService) {
  }

  public ngOnInit() {
    this.houseFeatures = this.houseService.getHouseFeatures();
  }
}
