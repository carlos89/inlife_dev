import { Component, OnInit } from '@angular/core';
import { DataService } from '../../../shared/services/data.service';

@Component({
  templateUrl: './personal-details.component.html',
  styleUrls: ['../register.component.scss'],
})
export class PersonalDetailsComponent implements OnInit {
  public formData = {
    nationality: null,
    gender: 'male',
    region: null,
    university: null,
  };

  public countries: any[] = [];
  public regions: any[] = [];
  public universities: any[] = [];

  constructor(private dataService: DataService) {
  }

  public ngOnInit() {
    this.countries = this.dataService.getCountries();
    this.regions = this.dataService.getRegions();
    this.universities = this.dataService.getUniversities();
  }
}
