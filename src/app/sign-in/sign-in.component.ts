import { Component, OnInit } from '@angular/core';
import { DataService } from '../shared/services/data.service';

@Component({
  selector: 'sign-in',
  styleUrls: ['./sign-in.component.scss'],
  templateUrl: './sign-in.component.html'
})
export class SignInComponent implements OnInit {
  public carouselItems: any[] = [
    {
      title: 'people',
      caption: {},
      url: 'https://loremflickr.com/600/600/people'
    },
    {
      title: 'city',
      caption: {},
      url: 'https://loremflickr.com/600/600/city'
    },
    {
      title: 'abstract',
      caption: {},
      url: 'https://loremflickr.com/600/600/abstract'
    }
  ];

  constructor(private dataService: DataService){
  }

  public ngOnInit() {
    const testimonials = this.dataService.getTestimonials();

    this.carouselItems = this.carouselItems.map((item: any, index) => {
      item.caption = testimonials[index];
      return item;
    });
  }
}
