import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { routes } from './sign-in.routes';
import { SignInComponent } from './sign-in.component';
import { LoginComponent } from './login';
import { RegisterComponent } from './register';
import { OnboardingComponent } from './onboarding';
import { OnboardingCardComponent } from './common/onboarding-card';
import { PersonalDetailsComponent } from './register/steps/personal-details.component';
import { TripDetailsComponent } from './register/steps/trip-details.component';
import { HouseDetailsComponent } from './register/steps/house-details.component';
import { CheckoutComponent } from './register/steps/checkout.component';
import { NouisliderModule } from 'ng2-nouislider';

@NgModule({
  declarations: [
    SignInComponent,
    LoginComponent,
    OnboardingComponent,
    OnboardingCardComponent,
    RegisterComponent,
    PersonalDetailsComponent,
    TripDetailsComponent,
    HouseDetailsComponent,
    CheckoutComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    NgbModule,
    NouisliderModule
  ]
})
export class SignInModule {
  public static routes = routes;
}
