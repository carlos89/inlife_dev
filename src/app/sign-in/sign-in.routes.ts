import { SignInComponent } from './sign-in.component';
import { LoginComponent } from './login';
import { RegisterComponent } from './register';
import { OnboardingComponent } from './onboarding';
import { PersonalDetailsComponent } from './register/steps/personal-details.component';
import { TripDetailsComponent } from './register/steps/trip-details.component';
import { HouseDetailsComponent } from './register/steps/house-details.component';
import { CheckoutComponent } from './register/steps/checkout.component';

export const routes = [
  {
    path: '',
    component: SignInComponent,
    children: [
      {path: '', component: LoginComponent},
      {path: 'onboarding', component: OnboardingComponent},
      {
        path: 'register',
        component: RegisterComponent,
        children: [
          {path: '', redirectTo: 'personal-details', pathMatch: 'full'},
          {path: 'personal-details', component: PersonalDetailsComponent},
          {path: 'trip-details', component: TripDetailsComponent},
          {path: 'house-details', component: HouseDetailsComponent},
          {path: 'checkout', component: CheckoutComponent},
        ]
      },
    ]
  }
];
